Run (all)!
It’s time to run and test all: iOS, Android and Web platform.
1. Run native server:
yarn start
2. Open a new terminal tab and run XCode Simulator with our new iOS app:
yarn run ios
Now you should see your app running on iOS.
3. Open an Android emulator (Android Studio > AVD Manager)
4. When the Android emulator is ready, run our new Android app:
yarn run android
Now you should see your app running on Android.
5. Finally, run also the web app:
yarn run web
Now you should see your app running on web on http://localhost:3000/.

